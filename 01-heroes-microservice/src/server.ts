import { writeDatabase, readDatabase } from './services/database.service'

writeDatabase([{ name: 'abc' }, { name: 'web' }, { name: 'opa' }])

console.log(readDatabase())
