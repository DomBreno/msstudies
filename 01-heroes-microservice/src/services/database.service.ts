import fs from 'fs'
import { dbConfig } from '../config'
import { isArray } from 'util'

function pipeline() {
  if (
    !fs.existsSync(`${dbConfig.name}.${dbConfig.ext}`) ||
    fs.readFileSync(`${dbConfig.name}.${dbConfig.ext}`).toString() === ''
  )
    return true
  else return false
}

export const createDatabase = () => {
  try {
    if (pipeline()) fs.writeFileSync(`${dbConfig.name}.${dbConfig.ext}`, '[]')
  } catch (error) {
    console.error(` \n Error on create database ${dbConfig.name}: \n ${error} `)
  }
}

export const readDatabase = () => {
  try {
    if (!pipeline()) return fs.readFileSync(`${dbConfig.name}.${dbConfig.ext}`).toString()

    createDatabase()

    return fs.readFileSync(`${dbConfig.name}.${dbConfig.ext}`).toString()
  } catch (error) {
    console.error(` \n Error on read database ${dbConfig.name}: \n ${error} `)
  }
}

export const writeDatabase = (data: object[] | object) => {
  try {
    let dataToDB: object[]

    if (isArray(data)) {
      dataToDB = data
    } else {
      dataToDB = [data]
    }

    fs.writeFileSync(`${dbConfig.name}.${dbConfig.ext}`, JSON.stringify(dataToDB), {
      flag: 'a'
    })
  } catch (error) {
    console.error(` \n Error on write database ${dbConfig.name}: \n ${error} `)
  }
}
